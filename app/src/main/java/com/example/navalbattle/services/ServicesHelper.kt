package com.example.navalbattle.services

import android.content.Context
import android.util.Log
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.Response.Listener
import com.android.volley.toolbox.JsonObjectRequest
import com.google.gson.Gson
import org.json.JSONObject

object ServicesHelper {

    private lateinit var queue: RequestQueue
    private val param = HashMap<String, String>()
    private lateinit var TAG: String
    private lateinit var listener: Listener<JSONObject>
    private lateinit var errorListener: Response.ErrorListener

    lateinit var TAG_ACT: String

    fun initWith(context: Context){
        queue= QueueSingleton.getInstance(context.applicationContext).requestQueue
        param["Content-Type"] = "application/json"
        TAG = this::class.java.simpleName

        listener = Listener { response ->
            Log.d(TAG, response.toString())
        }

        errorListener = Response.ErrorListener { error ->
            var body = String(error.networkResponse.data)
            Log.e(TAG, "" + error.networkResponse.statusCode)
            Log.e(TAG, body)
        }
    }


    fun join(
        json: Any, listen: Listener<JSONObject> = listener,
        errList: Response.ErrorListener = errorListener
    ) {
        commonPostRequest(EndPoint.JOIN, TAG_ACT, json, listen, errList)
    }


    fun leave(
        json: Any, listen: Listener<JSONObject> = listener,
        errList: Response.ErrorListener = errorListener
    ) {
        commonPostRequest(EndPoint.LEAVE, TAG_ACT, json, listen, errList)
    }


    fun attack(
        json: Any, listen: Listener<JSONObject> = listener,
        errList: Response.ErrorListener = errorListener
    ) {
        commonPostRequest(EndPoint.ATTACK, TAG_ACT, json, listen, errList)
    }

    fun map(
        json: Any, listen: Listener<JSONObject> = listener,
        errList: Response.ErrorListener = errorListener
    ) {
        commonRequest(Request.Method.GET, EndPoint.MAP, TAG_ACT, json, listen, errList)
    }


    private fun commonPostRequest(
        endPoint: String,
        TAG: String,
        json: Any,
        listen: Listener<JSONObject> = listener,
        errList: Response.ErrorListener = errorListener
    ) {
        commonRequest(Request.Method.POST, endPoint, TAG, json, listen, errList)
    }


    private fun commonRequest(
        method: Int,
        endPoint: String,
        TAG: String,
        json: Any,
        listen: Listener<JSONObject> = listener,
        errList: Response.ErrorListener = errorListener
    ) {

        var str: String = Gson().toJson(json)
        var jsonObject = JSONObject(str)

        val jsonObjectRequest = object : JsonObjectRequest(
            method,
            endPoint,
            jsonObject,
            listen,
            errList
        ) {
            override fun getHeaders(): MutableMap<String, String> {
                return param
            }
        }

        jsonObjectRequest.tag = TAG
        queue.add(jsonObjectRequest)

    }


    fun cancelAll(TAG: String) {
        queue?.cancelAll(TAG)
    }

}