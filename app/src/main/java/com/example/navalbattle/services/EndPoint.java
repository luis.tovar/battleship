package com.example.navalbattle.services;

public final class EndPoint {
    private EndPoint() {
    }

    public static final String URL;

    private static final String join;
    private static final String leave;
    private static final String attack;
    private static final String map;

    public static final String JOIN;
    public static final String LEAVE;
    public static final String ATTACK;
    public static final String MAP;

    static {
        URL = "https://f927cc4b-907f-444e-a7c7-81327735a0a3.mock.pstmn.io";

        join = "/game/join";
        leave = "/game/leave";
        attack = "/game/attack";
        map = "/game/map";

        JOIN = URL + join;
        LEAVE = URL + leave;
        ATTACK = URL + attack;
        MAP = URL + map;
    }

}
