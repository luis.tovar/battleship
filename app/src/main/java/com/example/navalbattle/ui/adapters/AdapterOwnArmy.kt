package com.example.navalbattle.ui.adapters

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import com.example.navalbattle.R
import com.example.navalbattle.models.ShipCell

class AdapterOwnArmy(private val myDataset: ArrayList<ShipCell>, var numberOfShips:Int,var isEnableClick:Boolean=false ) :
    RecyclerView.Adapter<AdapterOwnArmy.MyViewHolder>() {

    var clickItem: ClickItem? = null
    var counterShips = 0
    private var deadCounter = 0

    var attackedPlaces = ArrayList<Int>()


    interface ClickItem {
        fun doOnclick(shipCell: ShipCell)
        fun doOnLosing()
    }

    class MyViewHolder(var view: View) : RecyclerView.ViewHolder(view) {

        var container: ConstraintLayout
        var tV1: TextView
        var tV2: TextView
        var fMCross: FrameLayout
        var iVskull: ImageView


        init {
            container = view.findViewById(R.id.container)
            tV1 = view.findViewById(R.id.tV1)
            tV2 = view.findViewById(R.id.tV2)
            fMCross = view.findViewById(R.id.fMCross)
            iVskull = view.findViewById(R.id.iVskull)
        }
    }


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterOwnArmy.MyViewHolder {

        val textView = LayoutInflater.from(parent.context)
            .inflate(R.layout.cell_ship_view, parent, false) as View

        return MyViewHolder(textView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        var shipCell = myDataset.get(position)

        holder.tV1.text = myDataset[position].text
        holder.tV2.text = myDataset[position].text2


        if (shipCell.hasShip) {
            setShipView(holder)
        } else {
            cleanShipView(holder)
        }



        holder.view.setOnClickListener {
            if (isEnableClick) {
                if (!shipCell.hasShip && counterShips < numberOfShips) {
                    counterShips++
                    shipCell.hasShip = true
                    setShipView(holder)
                } else if (shipCell.hasShip) {
                    counterShips--
                    shipCell.hasShip = false
                    cleanShipView(holder)
                } else {
                    clickItem?.doOnclick(shipCell)
                }
            }

        }

        if (shipCell.hasShot) {

            if (shipCell.hasShip) {
                setShotSkullView(holder)
            } else {
                setShotView(holder)
            }

        }

    }

    fun receiveAttack(placce: Int) {
        attackedPlaces.add(placce)
        myDataset.get(placce).hasShot = true
        notifyItemChanged(placce)
        if(myDataset.get(placce).hasShip){
            deadCounter++
        }
        if(deadCounter == numberOfShips){
            clickItem?.doOnLosing()
        }
    }

    fun setShipView(holder: MyViewHolder) {
        holder.container.setBackgroundResource(R.drawable.ship_android)
    }

    fun cleanShipView(holder: MyViewHolder) {
        holder.container.setBackgroundResource(0)
    }

    fun setShotView(holder: MyViewHolder) {
        holder.fMCross.setBackgroundResource(R.drawable.shape_cross)
    }


    fun setShotSkullView(holder: MyViewHolder) {
        holder.container.setBackgroundResource(R.drawable.skull)
    }


    override fun getItemCount() = myDataset.size
}