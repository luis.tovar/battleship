package com.example.navalbattle.ui.adapters

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import com.example.navalbattle.R
import com.example.navalbattle.models.ShipCell

class AdapterEnemyArmy(private val myDataset: ArrayList<ShipCell>, var numberOfShips:Int, var isEnableClick:Boolean=false ) :
    RecyclerView.Adapter<AdapterEnemyArmy.MyViewHolder>() {

    var clickItem: ClickItem? = null

    private var counterShips = 0
    private var deadCounter = 0


    interface ClickItem {
        fun showAnimation(shipCell: ShipCell)
        fun doOnLosing()
        fun attack(place:Int)

    }

    class MyViewHolder(var view: View) : RecyclerView.ViewHolder(view) {

        var container: ConstraintLayout
        var tV1: TextView
        var tV2: TextView
        var fMCross: FrameLayout
        var iVskull: ImageView

        init {
            container = view.findViewById(R.id.container)
            tV1 = view.findViewById(R.id.tV1)
            tV2 = view.findViewById(R.id.tV2)
            fMCross = view.findViewById(R.id.fMCross)
            iVskull = view.findViewById(R.id.iVskull)
        }
    }


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterEnemyArmy.MyViewHolder {

        val textView = LayoutInflater.from(parent.context)
            .inflate(R.layout.cell_ship_view, parent, false) as View

        return MyViewHolder(textView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        var shipCell = myDataset.get(position)

        holder.tV1.text = myDataset[position].text
        holder.tV2.text = myDataset[position].text2

        if (shipCell.hasShot) {
            if(shipCell.hasShip){
                setShotSkullView(holder)
            }else{
                setShotView(holder)
            }
        }



        holder.view.setOnClickListener {

            if (isEnableClick) {

                if (!shipCell.hasShot) {

                    shipCell.hasShot = true
                    clickItem?.showAnimation(shipCell)

                    if (shipCell.hasShip) {
                        setShotSkullView(holder)
                        deadCounter++
                        if(deadCounter == numberOfShips){
                            clickItem?.doOnLosing()
                        }
                    }else{
                        setShotView(holder)
                    }

                    if(deadCounter < numberOfShips){
                        clickItem?.attack(position)
                    }

                }
            }

        }

    }

    fun setShotView(holder: MyViewHolder) {
        holder.fMCross.setBackgroundResource(R.drawable.shape_cross)
    }

    fun setShotSkullView(holder: MyViewHolder) {
        holder.container.setBackgroundResource(R.drawable.skull)
    }




    override fun getItemCount() = myDataset.size
}