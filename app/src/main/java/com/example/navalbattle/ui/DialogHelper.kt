package com.example.navalbattle.ui

import android.app.Dialog
import android.support.v7.app.AppCompatActivity
import android.view.ViewGroup
import android.widget.Button
import com.example.navalbattle.R


class DialogHelper(var activity:AppCompatActivity){

    /*
    https://stackoverflow.com/questions/40398072/singleton-with-parameter-in-kotlin
    */
    companion object {

        @Volatile
        private var INSTANCE: DialogHelper? = null

        fun getInstance(activity: AppCompatActivity): DialogHelper =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(activity).also { INSTANCE = it }
            }

        private fun buildDatabase(activity: AppCompatActivity) = DialogHelper(activity)

    }

    private lateinit var dialog:Dialog
    private fun simpleDialog(layoutResourse:Int) {
        dialog = Dialog(activity)
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);


        dialog.setContentView(layoutResourse)

        var btnDialog = dialog.findViewById<Button>(R.id.btnDialog)
        btnDialog.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()

        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
    }

    fun dialogLost(){
        simpleDialog(R.layout.dialog_lost)
    }

    fun dialogWin(){
        simpleDialog(R.layout.dialog_win)
    }


}
