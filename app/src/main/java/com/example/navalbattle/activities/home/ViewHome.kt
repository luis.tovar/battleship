package com.example.navalbattle.activities.home

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.example.navalbattle.R
import com.example.navalbattle.activities.BaseView
import com.example.navalbattle.activities.main.ViewMain
import kotlinx.android.synthetic.main.activity_view_home.*

class ViewHome : BaseView() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_home)
        init()
    }

    private fun init() {

        btnPlay.setOnClickListener {
            val intent = Intent(context, ViewMain::class.java)
            startActivity(intent)
        }

        btnPlayOnline.setOnClickListener {
            Toast.makeText(context,"Functionality in construction",Toast.LENGTH_SHORT).show()
        }

    }
}
