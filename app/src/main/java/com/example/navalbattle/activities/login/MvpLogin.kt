package com.example.navalbattle.activities.login

interface MvpLogin {

    interface View {
        fun initView()
        fun showLoading()
        fun hideLoading()
        fun disableButtons()
        fun enableButtons()
    }

    interface Presenter {
        fun hideLoading()
    }

    interface Model {

    }

}