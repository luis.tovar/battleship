package com.example.navalbattle.activities.main

import android.util.Log
import com.android.volley.Response
import com.example.navalbattle.services.ServicesHelper
import com.example.navalbattle.models.request.AttackRequests
import com.example.navalbattle.models.request.JoinRequest
import com.example.navalbattle.models.request.LeaveRequest
import com.example.navalbattle.models.request.MapRequest

class RepositoryMain(
    private var presenter: MvpMain.Presenter,
    private var TAG: String
) : MvpMain.Model {


    init {
        ServicesHelper.TAG_ACT = TAG;
    }

    override fun join() {

        ServicesHelper.join(
            JoinRequest(),
            Response.Listener { response ->
                //TODO define behavior
                Log.d(TAG, "From Rep Sigleton" + response.toString())
                presenter.hideLoading()

            },
            Response.ErrorListener { error ->
                //TODO define behavior
//                var body = String(error.networkResponse.data)
//                Log.e(TAG, "" + error.networkResponse.statusCode)
//                Log.e(TAG, body)
                presenter.hideLoading()
            })

    }

    override fun leave() {
        ServicesHelper.leave(
            LeaveRequest(),
            Response.Listener { response ->
                //TODO define behavior
                Log.d(TAG, "From Rep" + response.toString())
                presenter.hideLoading()

            },
            Response.ErrorListener { error ->
                //TODO define behavior
                var body = String(error.networkResponse.data)
                Log.e(TAG, "" + error.networkResponse.statusCode)
                Log.e(TAG, body)
                presenter.hideLoading()
            })
    }

    override fun attack() {
        ServicesHelper.attack(
            AttackRequests(),
            Response.Listener { response ->
                //TODO define behavior
                Log.d(TAG, "From Rep" + response.toString())
                presenter.hideLoading()

            },
            Response.ErrorListener { error ->
                //TODO define behavior
                var body = String(error.networkResponse.data)
                Log.e(TAG, "" + error.networkResponse.statusCode)
                Log.e(TAG, body)
                presenter.hideLoading()
            })
    }

    override fun map() {

        ServicesHelper.map(
            MapRequest(),
            Response.Listener { response ->
                //TODO define behavior
                Log.d(TAG, "From Rep" + response.toString())
                presenter.hideLoading()

            },
            Response.ErrorListener { error ->
                //TODO define behavior
                var body = String(error.networkResponse.data)
                Log.e(TAG, "" + error.networkResponse.statusCode)
                Log.e(TAG, body)
                presenter.hideLoading()
            })
    }

    override fun cancelAll() {
        ServicesHelper.cancelAll(TAG)
    }

}