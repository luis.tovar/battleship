package com.example.navalbattle.activities.main

interface MvpMain {

    interface View {

        fun showLoad()
        fun hideLoad()

    }

    interface Presenter {
        fun join()
        fun leave()
        fun attack()
        fun map()
        fun cancelAll()

        fun hideLoading()
    }

    interface Model {
        fun join()
        fun leave()
        fun attack()
        fun map()
        fun cancelAll()
    }

}