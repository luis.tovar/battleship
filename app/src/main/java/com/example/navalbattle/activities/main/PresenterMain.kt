package com.example.navalbattle.activities.main


class PresenterMain(private var view: MvpMain.View, private var TAG: String) :
    MvpMain.Presenter {


    override fun hideLoading() {
        view.hideLoad()
    }

    private var repository: MvpMain.Model = RepositoryMain(this, TAG)

    override fun join() {
        view.showLoad()
        repository.join()
    }

    override fun leave() {
        view.showLoad()
        repository.leave()
    }

    override fun attack() {
        view.showLoad()
        repository.attack()
    }

    override fun map() {
        view.showLoad()
        repository.map()
    }

    override fun cancelAll() {
        view.hideLoad()
        repository.cancelAll()
    }
}