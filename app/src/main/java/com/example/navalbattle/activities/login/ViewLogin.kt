package com.example.navalbattle.activities.login

import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.View
import com.example.navalbattle.R
import com.example.navalbattle.activities.BaseView
import com.example.navalbattle.activities.home.ViewHome
import com.example.navalbattle.activities.register.ViewRegister
import kotlinx.android.synthetic.main.activity_view_login.*

/*
https://www.androidhive.info/2012/01/android-login-and-registration-with-php-mysql-and-sqlite/
*/

class ViewLogin : BaseView() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_login)

        init()
    }

    private fun init() {
        context = this
        btnLogin.setOnClickListener {
            val intent = Intent(context, ViewHome::class.java)
            startActivity(intent)
        }

        btnLinkToRegisterScreen.setOnClickListener {
            val intent = Intent(context, ViewRegister::class.java)
            startActivity(intent)
        }

        email.setOnFocusChangeListener(View.OnFocusChangeListener { view, b -> hasFocus(b) })


    }

    private fun hasFocus(b: Boolean) {
        if(b){

//            tILemail.setHintTextAppearance(ContextCompat.getColor(context, R.color.white))
        }else{
//            email.setHintTextColor(ContextCompat.getColor(context, R.color.input_login_hint))
        }
    }



}
