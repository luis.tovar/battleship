package com.example.navalbattle.activities.main

import android.animation.Animator
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.Toast
import com.example.navalbattle.R
import com.example.navalbattle.activities.BaseView
import com.example.navalbattle.models.ShipCell
import com.example.navalbattle.ui.adapters.AdapterEnemyArmy
import com.example.navalbattle.ui.adapters.AdapterOwnArmy
import com.example.navalbattle.ui.adapters.AdapterOwnArmy.ClickItem
import kotlinx.android.synthetic.main.activity_view_main.*
import java.util.*


class ViewMain : BaseView(), MvpMain.View {


    private lateinit var presenter: MvpMain.Presenter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_main)

        presenter = PresenterMain(this, TAG)
        init()
    }


    private var isPlaying: Boolean = false
    private fun init() {

        isPlaying = false
        initArmy()
        initEnemy()

        btnPlay.setOnClickListener { playClick() }

        tVStatus.visibility =View.GONE
        btnPlay.text = getString(R.string.play)
    }

    private fun playClick() {
        if (viewAdapter?.counterShips == numberOfShips) {
            initPlay()
        } else {
            Toast.makeText(context, R.string.not_enough, Toast.LENGTH_SHORT).show()
        }
    }

    private fun initPlay() {
        isPlaying = true
        viewAdapter?.isEnableClick = !isPlaying
        viewAdapterEnemy?.isEnableClick = isPlaying
        btnPlay.setEnabled(false)
        btnPlay.setAlpha(0.3f)
        recyclerEnemy.visibility = View.VISIBLE
        tVEnemy.visibility = View.VISIBLE
    }



    private lateinit var recyclerView: RecyclerView
    private var viewAdapter: AdapterOwnArmy? = null

    private val numberOfShips = 5
    private val numberOfCells = 24

    private fun initArmy() {

        var myDataset = ArrayList<ShipCell>()

        for (i: Int in 1..numberOfCells) {
            myDataset.add(ShipCell(i))
        }
        viewAdapter?.counterShips = 0

        viewAdapter = AdapterOwnArmy(myDataset, numberOfShips)
        viewAdapter?.isEnableClick = !isPlaying


        recyclerView = findViewById<RecyclerView>(R.id.my_recycler_view).apply {
            setHasFixedSize(true)
            adapter = viewAdapter
        }

        viewAdapter?.clickItem = object : ClickItem {
            override fun doOnLosing() {
                doOnLostUser()
            }

            override fun doOnclick(shipCell: ShipCell) {
                if(viewAdapter?.counterShips == numberOfShips){
                    Toast.makeText(context, R.string.press_play_button, Toast.LENGTH_SHORT).show()
                }

            }
        }

    }

    var random = Random()
    private fun attackAct(place: Int) {
        var plac = random.nextInt(numberOfCells)

        while (viewAdapter?.attackedPlaces!!.contains(plac) || plac == 0) {
            plac = random.nextInt(numberOfCells)
        }

        viewAdapter?.receiveAttack(plac)


    }


    private lateinit var recyclerEnemy: RecyclerView
    private var viewAdapterEnemy: AdapterEnemyArmy? = null

    private fun initEnemy() {

        var myDatasetEnemy = ArrayList<ShipCell>()

//        var num = intArrayOf(2, 6, 10, 15, 23).toList()
        var num = intArrayOf( 20, 21, 22, 23, 24).toList()
        for (i: Int in 1..numberOfCells) {
            myDatasetEnemy.add(
                ShipCell(
                    i, "", "",
                    num.contains(i), false
                )
            )
        }

        viewAdapterEnemy = AdapterEnemyArmy(myDatasetEnemy, numberOfShips)

        viewAdapterEnemy?.clickItem = object : AdapterEnemyArmy.ClickItem {
            override fun attack(place: Int) {
                attackAct(place)
            }


            override fun doOnLosing() {
                doOnWinUser()
            }

            override fun showAnimation(shipCell: ShipCell) {
                presenter.join()
            }
        }


        recyclerEnemy = findViewById<RecyclerView>(R.id.recyclerEnemy).apply {
            setHasFixedSize(true)
            adapter = viewAdapterEnemy
        }

        recyclerEnemy.visibility = View.GONE
        tVEnemy.visibility = View.GONE
    }


    private fun doOnWinUser(){
        DH.dialogWin()
        viewAdapterEnemy?.isEnableClick = false
        tVStatus.text = context.getText(R.string.you_win)
        tVStatus.setTextColor(ContextCompat.getColor(context, android.R.color.holo_green_light))
        tVStatus.visibility = View.VISIBLE

        restart()
    }
    private fun restart(){
        btnPlay.text= context.getString(R.string.restart)
        btnPlay.setEnabled(true)
        btnPlay.setAlpha(1.0f)

        btnPlay.setOnClickListener { init() }

    }


    private fun doOnLostUser(){
        DH.dialogLost()
        viewAdapterEnemy?.isEnableClick = false
        tVStatus.text = context.getText(R.string.you_lost)
        tVStatus.setTextColor(ContextCompat.getColor(context, android.R.color.holo_red_light))
        tVStatus.visibility = View.VISIBLE

        restart()
    }


    override fun showLoad() {
        animation_view.visibility = View.VISIBLE
        animation_view.setRepeatCount(1)
        animation_view.playAnimation()

        animation_view.addAnimatorListener( object :Animator.AnimatorListener{
            override fun onAnimationRepeat(animation: Animator?) {
                //TODO("not implemented")
            }

            override fun onAnimationEnd(animation: Animator?) {
                //TODO("not implemented")
                animation_view.visibility = View.GONE
            }

            override fun onAnimationCancel(animation: Animator?) {
                //TODO("not implemented")
            }

            override fun onAnimationStart(animation: Animator?) {
                //TODO("not implemented")
            }
        })


    }

    override fun hideLoad() {
    }

    override fun onStop() {
        super.onStop()
        presenter.cancelAll()
    }


}
