package com.example.navalbattle.activities.register

import android.content.Intent
import android.os.Bundle
import com.example.navalbattle.R
import com.example.navalbattle.activities.BaseView
import com.example.navalbattle.activities.home.ViewHome
import kotlinx.android.synthetic.main.activity_view_register.*




class ViewRegister : BaseView() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_register)

        init()
    }

    private fun init() {
        btnLinkToLoginScreen.setOnClickListener {
            onBackPressed()
        }

        btnRegister.setOnClickListener {

            val intent = Intent(context, ViewHome::class.java)
            startActivity(intent)
        }
    }
}
