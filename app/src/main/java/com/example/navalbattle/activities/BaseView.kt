package com.example.navalbattle.activities

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.example.navalbattle.R
import com.example.navalbattle.ui.DialogHelper

abstract class BaseView : AppCompatActivity() {

    protected lateinit var TAG: String
    protected lateinit var context: Context
    protected lateinit var DH: DialogHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        TAG = this::class.java.simpleName
        context = this
        DH = DialogHelper.getInstance(this)
        DH.activity = this
    }

    lateinit var progressBar: ProgressBar

    override fun onResume() {
        super.onResume()


        if (null == findViewById(R.id.progressBar)) {
            var view = this.findViewById<ViewGroup>(android.R.id.content)
            View.inflate(this, R.layout.loading, view)
            progressBar = findViewById(R.id.progressBar)
        }
    }

    protected fun showLoading() {
        progressBar.visibility = View.VISIBLE
    }

    protected fun hideLoading() {
        progressBar.visibility = View.GONE
    }
}