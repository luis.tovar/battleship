package com.example.navalbattle

import android.app.Application
import com.example.navalbattle.services.EndPoint
import com.example.navalbattle.services.ServicesHelper

class MyApp : Application() {

    override fun onCreate() {
        super.onCreate()
        ServicesHelper.initWith(this)
    }


}